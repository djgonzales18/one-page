var express = require('express');
var hbs  = require('express-handlebars');

var app = express();

//settings
app.engine('.hbs', hbs({
    defaultLayout: 'main', 
    extname: '.hbs', 
    partialsDir: ['views/partials/']
}));

app.set('view engine', '.hbs');

app.use(express.static('public'));

//routes
app.get('/', function (req, res) {
    res.render('home');
});

//server
app.listen(3000, () => {
    console.log('Servidor iniciado...');
});